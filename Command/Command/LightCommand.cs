﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Command.Systems;

namespace Command.Command
{
    public class LightCommand : ICommand
    {

        private Light _light;
        private LightsState _prevState;
        private Stack<LightsState> _states; 
        public LightCommand(Light light)
        {
            _light = light;
            _states = new Stack<LightsState>();
        }
        public void Execute()
        {
            _states.Push(_light._state);
            _light.ToggleLight();
        }

        public void Undo()
        {
            var prevState = _states.Pop();
            _light.SetState(prevState);
        }

        public override string ToString()
        {
            return "Включить свет";
        }
    }
}
