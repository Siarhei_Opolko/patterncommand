﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Command.Systems;

namespace Command.Command
{
    public class TeapotCommand : ICommand
    {
        private Teapot _teapot;

        public TeapotCommand(Teapot teapot)
        {
            _teapot = teapot;
        }
        public void Execute()
        {
            _teapot.TurnOn();
        }

        public void Undo()
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return "Включить чайник";
        }
    }
}
