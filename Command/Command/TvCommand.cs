﻿using System;
using Command.Systems;

namespace Command.Command
{
    public class TvCommand : ICommand
    {
         private Tv _tv;

         public TvCommand(Tv tv)
        {
            _tv = tv;
        }
        public void Execute()
        {
            _tv.TurnOn();
        }

        public void Undo()
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return "Включить ТВ";
        }
    }
}
