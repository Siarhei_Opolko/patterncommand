﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Command.Command;
using Command.Systems;

namespace Command
{
    class Program
    {
        static void Main(string[] args)
        {
            var control = new RemoteControl();
            string @string = null;

            control.SetCommandForButton(1, new LightCommand(new Light()));
            control.SetCommandForButton(2, new TvCommand(new Tv()));
            control.SetCommandForButton(3, new MusicCommand(new Music()));
            control.SetCommandForButton(4, new TeapotCommand(new Teapot()));

            do
            {
                Console.WriteLine("Выберите вариант:");
                Console.WriteLine(control);

                Console.Write("\nВаш выбор: ");
                var input = Console.ReadLine();
                int buttonId;

                int.TryParse(input, out buttonId);

                control.PushButton(buttonId);
                control.PushButton(buttonId);
                control.PushButton(buttonId);
                control.PushButton(buttonId);
                control.PushButton(buttonId);
                control.PushButton(buttonId);
                control.UndoButton(buttonId);
                control.UndoButton(buttonId);
                control.UndoButton(buttonId);
                Console.WriteLine("Продолжить y/n?");
                @string = Console.ReadLine();
            } while (@string == "y");
        }
    }
}
