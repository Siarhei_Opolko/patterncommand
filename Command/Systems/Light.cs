﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command.Systems
{
    public enum State
    {
        On = 1,
        Off = 0
    }

    public enum LightsState
    {
        High = 3,
        Medium = 2,
        Low = 1,
        Off = 0
    }
    public class Light
    {
        public LightsState _state { get; set; }
        public void ToggleLight()
        {
            switch (_state)
            {
                case LightsState.Off: 
                    _state = LightsState.Low;
                    break;
                case LightsState.Low: 
                    _state = LightsState.Medium;
                    break;
                case LightsState.Medium: 
                    _state = LightsState.High;
                    break;
                case LightsState.High: 
                    _state = LightsState.Off; 
                    break;
            }

            Print();
        }

        public void TurnOff()
        {
            _state = LightsState.Off;
            Print();
        }

        public void SetState(LightsState state)
        {
            _state = state;
            Print();
        }

        private void Print()
        {
            switch (_state)
            {
                case LightsState.Off:
                    Console.WriteLine("Свет выключен");
                    break;

                case LightsState.Low:
                    Console.WriteLine("Свет тусклый");
                    break;

                case LightsState.Medium:
                   Console.WriteLine("Свет средний");
                    break;

                case LightsState.High:
                    Console.WriteLine("Свет яркий");
                    break;    
            }
        }
    }
}
