﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command.Systems
{
    public class Music
    {
        public State _state { get; set; }
        public void TurnOn()
        {
            Console.WriteLine("Музыка включена");
            _state = State.On;
        }

        public void TurnOff()
        {
            Console.WriteLine("Музыка выключена");
            _state = State.Off;
        }
    }
}
