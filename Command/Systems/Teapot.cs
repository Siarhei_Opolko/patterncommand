﻿using System;

namespace Command.Systems
{
    public class Teapot
    {
        private State _state { get; set; }
        public void TurnOn()
        {
            Console.WriteLine("Чайник включен");
            _state = State.On;
        }

        public void TurnOff()
        {
            Console.WriteLine("Чайник выключен");
            _state = State.Off;
        }
    }
}
