﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command.Systems
{
    public class Tv
    {
        public State _state { get; set; }
        public void TurnOn()
        {
            Console.WriteLine("Телевизор включен");
            _state = State.On;
        }

        public void TurnOff()
        {
            Console.WriteLine("Телевизор выключен");
            _state = State.Off;
        }
    }
}
